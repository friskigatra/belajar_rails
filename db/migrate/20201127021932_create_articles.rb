class CreateArticles < ActiveRecord::Migration[6.0]
  def change
    create_table :articles do |t|

      t.belongs_to :user, index: true
      
      t.string :title
      t.string :content
      t.string :status
      t.boolean :is_published, default: false
      t.datetime :published_at

      t.timestamps
    end
  end
end
