Rails.application.routes.draw do
  scope :api, defaults: { format: :json } do
    scope :v1 do
      devise_for :users,
                  path: '',
                  path_names: {
                    sign_in: 'login',
                    sign_out: 'logout',
                    registration: 'signup'
                  },
                  controllers: {
                    sessions: 'api/v1/users/sessions',
                    registrations: 'api/v1/users/registrations',
                  }
    end
  end

  namespace 'api', defaults: { format: :json } do
    namespace 'v1' do
      resources :articles
    end
  end
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end