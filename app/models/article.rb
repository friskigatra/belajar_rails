class Article < ApplicationRecord

    belongs_to :user
    
    validates :user_id, presence: { message: "User Id harus terisi." }
    validates :title, presence: { message: "Judul Post harus terisi." },
                      length: { 
                          minimum: 6,
                          message: "Judul Post minimal terdiri dari 6 karakter."
                        }
    validates :content, presence: { message: "Isi Post harus terisi." },
                        length: { 
                            minimum: 6, 
                            message: "Isi Post minimal terdiri dari 6 karakter."
                        }
    validates :status, presence: { message: "Status harus terisi." },
                        inclusion: { 
                            in: ["Draft", "Published"],
                            message: "Pilihan status adalah Draft dan Published saja."
                        }
end
