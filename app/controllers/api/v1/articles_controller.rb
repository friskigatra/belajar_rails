class Api::V1::ArticlesController < ApplicationController

    before_action :authenticate_user!, except: [:index, :show]

    def index
      articles = Article.all
      render json: articles
    end
  
    def show
      #article = article.find_by(id: params[:id])
      article = Article.find_by_id(params[:id])
      if article
        render json: article
      else
        render json: { message: "data tidak ditemukan" }
      end
    end
  
    def create
  
      article = Article.new
  
      article.title = params[:title]
      article.content = params[:content]
      article.status = params[:status]
      article.is_published = params[:status] == "Draft" ? false : true
      article.user_id = 1
  
      if article.save
        render json: { message: "sukses" }
      else
        render json: { errors: article.errors }
      end
    end
  
    def update
      article = Article.find_by_id(params[:id])
      article.title = params[:title] if params[:title]
      article.content = params[:content] if params[:content]
      article.status = params[:status] if params[:status]
      if params[:status]
        article.is_published = params[:status] == "Draft" ? false : true
      end
      if article.save
        render json: article
      else
        render json: { message: "data tidak ditemukan" }
      end
    end
  
    def destroy
      article = Article.find_by_id(params[:id])
      if article.delete
        render json: { message: "hapus sukses" }
      else
        render json: { message: "data tidak ditemukan" }
      end
    end
end
