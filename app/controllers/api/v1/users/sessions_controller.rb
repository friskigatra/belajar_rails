# frozen_string_literal: true

class Api::V1::Users::SessionsController < Devise::SessionsController

  private

    # POST /api/v1/login
    def respond_with(resource, _opts = {})
      render json: resource
    end

    # DELETE /api/v1/logout
    def respond_to_on_destroy
      head :no_content
    end
end
