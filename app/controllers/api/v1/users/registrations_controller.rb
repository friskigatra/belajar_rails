# frozen_string_literal: true

class Api::V1::Users::RegistrationsController < Devise::RegistrationsController

  # POST /api/v1/signup
  def create
    build_resource(sign_up_params)
    resource.save
    render_resource(resource)
  end

  private

    def sign_up_params
      params.permit(:email, :password, :name)
    end
end
